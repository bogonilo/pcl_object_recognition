#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>

/*
 * PUBLISHER DI CONTROLLO PER CLUSTER E PCL FILTRATE
 * */

main(int argc, char **argv) {
  ros::init (argc, argv, "pcl_read_clus");
  ros::NodeHandle nh;
  ros::Publisher pcl_pub = nh.advertise<sensor_msgs::PointCloud2> ("pcl_output_cluster", 1);
  sensor_msgs::PointCloud2 output;
  pcl::PointCloud<pcl::PointXYZRGBA> cloud;
  std::stringstream ss;

  //ss << "/home/lorenzo/ros_ws/cloud_cluster_" << argv[1] << ".pcd";
   ss << "/home/lorenzo/ros_ws/test_fil.pcd";
  const std::string file = ss.str();
  pcl::io::loadPCDFile (file, cloud);
  pcl::toROSMsg(cloud, output);
  output.header.frame_id = "/camera_rgb_optical_frame";
  ros::Rate loop_rate(1);
  while (ros::ok()) {
    pcl_pub.publish(output);
    ros::spinOnce();
    loop_rate.sleep();
}
return 0;
}



























/*#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/plane_clipper3D.h>
#include <pcl/filters/passthrough.h>
#include <pcl/kdtree/kdtree.h>


  //PCL READER

main(int argc, char **argv) {

  ros::init (argc, argv, "pcl_read");
  ros::NodeHandle nh;
  ros::Publisher pcl_pub = nh.advertise<sensor_msgs::PointCloud2> ("pcl_output", 1);
  sensor_msgs::PointCloud2 output;

  pcl::PointCloud<pcl::PointXYZ> cloudInput;
  pcl::io::loadPCDFile ("/home/daniele/ros_ws/test.pcd", cloudInput);

  pcl::PointCloud<pcl::PointXYZ> cloudMod;
  // Fill in the cloud data
  cloudMod.width = cloudInput.width;
  cloudMod.height = cloudInput.height;
  cloudMod.points.resize(cloudInput.width * cloudInput.height);
  float maxX = -100;
  float minX = +100;

  for (size_t i = 0; i < cloudInput.points.size(); ++i) {
    /*if(cloudInput.points[i].x < minX)
      minX = cloudInput.points[i].x;
    if(cloudInput.points[i].x > maxX)
      maxX = cloudInput.points[i].x;
      */

/*    if(cloudInput.points[i].x < 0.4 && cloudInput.points[i].x > -0.4){
      cloudMod.points[i].x = cloudInput.points[i].x;
      cloudMod.points[i].y = cloudInput.points[i].y;
      cloudMod.points[i].z = cloudInput.points[i].z;
      //ROS_INFO("\nZ: %f", cloud.points[i].z);
    }
  }

//ROS_INFO("MinX-Reale: %f, MaxX-Reale: %f", -2.432674, 2.302935);
 // ROS_INFO("MinX-Simul: %f, MaxX-Simul: %f", minX, maxX);


  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

    // Fill in the cloud data
    cloud->width  = cloudMod.width;
    cloud->height = cloudMod.height;
    cloud->points.resize (cloud->width * cloud->height);

    for (size_t i = 0; i < cloudMod.points.size(); ++i) {

        cloud->points[i].x = cloudMod.points[i].x;
        cloud->points[i].y = cloudMod.points[i].y;
        cloud->points[i].z = cloudMod.points[i].z;

    }


  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZ> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (0.01);

  seg.setInputCloud (cloud);
  seg.segment (*inliers, *coefficients);

  if (inliers->indices.size () == 0)
  {
    PCL_ERROR ("Could not estimate a planar model for the given dataset.");
    return (-1);
  }

  std::cerr << "Model coefficients: " << coefficients->values[0] << " "
                                      << coefficients->values[1] << " "
                                      << coefficients->values[2] << " "
                                      << coefficients->values[3] << std::endl;

  pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud (cloud);
    pass.setFilterFieldName ("z");
    pass.setFilterLimits (0.0, 1.0);
    //pass.setFilterLimitsNegative (true);
    pass.filter (*cloud_filtered);


    pcl::PointCloud<pcl::PointXYZ> cloudMod1;
    // Fill in the cloud data
    cloudMod1.width = cloudInput.width;
    cloudMod1.height = cloudInput.height;
    cloudMod1.points.resize(cloudInput.width * cloudInput.height);

    std::cerr << "Cloud after filtering: " << std::endl;
    for (size_t i = 0; i < cloud_filtered->points.size (); ++i){
      std::cerr << "    " << cloud_filtered->points[i].x << " "
                          << cloud_filtered->points[i].y << " "
                          << cloud_filtered->points[i].z << std::endl;

        cloudMod1.points[i].x = cloud_filtered->points[i].x;
        cloudMod1.points[i].y = cloud_filtered->points[i].y;
        cloudMod1.points[i].z = cloud_filtered->points[i].z;

    }

    // Creating the KdTree object for the search method of the extraction
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud (cloud_filtered);



  pcl::toROSMsg(cloudMod1, output);
  output.header.frame_id = "world";

  ros::Rate loop_rate(1);

  while (ros::ok()) {
    pcl_pub.publish(output);
    ROS_INFO("[%s]", "pubblico nel topic");
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;

}

/* PUNTI A CASO
 *
main (int argc, char **argv) {
  ros::init (argc, argv, "pcl_create");
  ros::NodeHandle nh;
  ros::Publisher pcl_pub = nh.advertise<sensor_msgs::PointCloud2> ("pcl_output", 1);
  sensor_msgs::PointCloud2 output;

  pcl::PointCloud<pcl::PointXYZ> cloud;
  // Fill in the cloud data
  cloud.width = 100;
  cloud.height = 1;
  cloud.points.resize(cloud.width * cloud.height);
  for (size_t i = 0; i < cloud.points.size (); ++i) {
    cloud.points[i].x = 1024 * rand () / (RAND_MAX + 1.0f);
    cloud.points[i].y = 1024 * rand () / (RAND_MAX + 1.0f);
    cloud.points[i].z = 1024 * rand () / (RAND_MAX + 1.0f);
  }
  //Convert the cloud to ROS message
  pcl::toROSMsg(cloud, output);
  output.header.frame_id = "odom";
  ros::Rate loop_rate(1);
  while (ros::ok()) {
    pcl_pub.publish(output);
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}



Hi, I am trying to remove a plane of a table of my pointcloud.
Firstly I read the pointcloud from a file and then I calculated the model coefficient and this is the four coefficients:
  0.0587782
  -0.899725
  0.432481
  1.21072e-08

Now, how can I remove this plane from my pointcloud?
*/
