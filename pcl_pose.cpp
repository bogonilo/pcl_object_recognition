#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <math.h>
#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/registration/icp.h>
#include <tf/transform_broadcaster.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <sensor_msgs/PointCloud2.h>

/*
 * SAVE PCL ON FILE
 * */

void cloudCB(const sensor_msgs::PointCloud2 &input) {
  ROS_INFO("I heard something");
  count++;
  pcl::PointCloud<pcl::PointXYZRGBA> cloud ;
  pcl::fromROSMsg(input, cloud);
  pcl::io::savePCDFileASCII ("testRGBA.pcd", cloud);
}



main (int argc, char **argv){
  ros::init (argc, argv, "pcl_write");
  ros::NodeHandle nh;

  //simulato
  ros::Subscriber bat_sub = nh.subscribe("/camera/depth/points", 100, cloudCB);

  //reale
  //ros::Subscriber bat_sub = nh.subscribe("/camera/generated/lowres_points", 100, cloudCB);

  ros::spin();

  return 0;
}

