#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <math.h>
#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/registration/icp.h>
#include <tf/transform_broadcaster.h>


/*
 * CLUSTER COLOR RECOGNITION AND OBJECT RECOGNITION 
 *
 * COMPUTE THE CLUSTERS' CENTROID
 * OBJECT ORIENTATION COMPUTING WITH ICP ON A REFERENCE SUBJECT (SAMPLE A "STRAIGHT" CUBE AND
 *  "STRAIGHT" PRISM)
 *
 * BROADCAST NEW COMPUTED TFs 
 * */

int CalculateDistance(int r1, int g1, int b1, int r2, int g2, int b2)
{
   int diffr = r1 - r2;
   int diffg = g1 - g2;
   int diffb = b1 - b2;
   int diffr_sqr = diffr*diffr;
   int diffg_sqr = diffg*diffg;
   int diffb_sqr = diffb*diffb;
   int distance = sqrt(diffr_sqr + diffg_sqr + diffb_sqr);
   return distance;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "pcl_pose_computing");
  ros::NodeHandle nh;

  std::ifstream fileInput("n_cluster.txt");
  std::string s;
  std::getline(fileInput, s);
  fileInput.close();
  int n_cluster = std::stoi(s);
  ROS_INFO("\n Numero cluster : %d ", n_cluster);
  bool daAgg[n_cluster];

  tf::Transform transform[n_cluster];
  tf::TransformBroadcaster br[n_cluster];

  // vettore di cluster trovati
  std::vector <pcl::PointCloud<pcl::PointXYZRGBA>::Ptr, Eigen::aligned_allocator<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr > > cloudInput;

  for(int i = 0; i<n_cluster; i++)
  {
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
    cloudInput.push_back(cloud);
    std::stringstream ss;
    ss << "/home/lorenzo/ros_ws/cloud_cluster_" << i << ".pcd";
    pcl::io::loadPCDFile (ss.str() , *cloudInput[i]);
  }

  std::ofstream fileOutput("tfOggPCL.txt");
  std::string arrayNomi[n_cluster];

for (int n = 0; n < n_cluster; n++)  // mega for per tutti i cluster rilevati
{

  //*************************  RICONOSCIMENTO COLORE ******************************
  std::stringstream nome;
  bool cubo = false;
  bool prisma = false;
  int r = 0;
  int g = 0;
  int b = 0;
  for (size_t i = 0; i < cloudInput[n]->points.size(); ++i)
  {
    r = r + cloudInput[n]->points[i].r;
    g = g + cloudInput[n]->points[i].g;
    b = b + cloudInput[n]->points[i].b;
  }
  r = r/cloudInput[n]->points.size();
  g = g/cloudInput[n]->points.size();
  b = b/cloudInput[n]->points.size();
  ROS_INFO("\nColore rilevato: %d, %d, %d", r, g, b);

  //cilindro = 1, 134, 134 (simulato) 255, 255, 0 (reale)
  //cubo = 1, 1, 190 (simulato) 255, 0, 0 (reale)
  //prisma = bo 0, 255, 0 (reale)
  int dist1 = CalculateDistance(r, g, b, 255, 255, 0);
  int dist2 = CalculateDistance(r, g, b, 255, 0, 0);
  int dist3 = CalculateDistance(r, g, b, 0, 255, 0); // valori da aggiornare
  //int dist3 = 10000;

  if(dist1 < dist2)
  {
    if(dist1 < dist3)
      nome << "cilindro_" << n;
    else
    {
      nome << "prisma_" << n;
      prisma = true;
    }
  }
  else if (dist2 < dist3)
  {
    nome << "cubo_" << n;
    cubo = true;
  }
  else
  {
    nome << "prisma_" << n;
    prisma = true;
  }

  arrayNomi[n] = nome.str();

  //****************************** CALCOLO CENTROIDE *********************************

  Eigen::Vector4f centroid;
  pcl::compute3DCentroid(*cloudInput[n], centroid);
  std::cout << "Centroid of the Cluster "<< nome.str() << ": X = " << centroid[0] << " Y = " << centroid[1] << " Z = " << centroid[2] << std::endl;
  transform[n].setOrigin( tf::Vector3(centroid[0], centroid[1], centroid[2]) );
  transform[n].setRotation( tf::Quaternion(0.7, -0.7, 0.15, 0.18) );

  // ************************* ICP PER CALCOLO ORIENTAZIONE ***********************
  //if(centroid[0] < 0.25 && centroid[1] < 0.20){
    daAgg[n] = true;
  if (cubo)
  {
     pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_cubo (new pcl::PointCloud<pcl::PointXYZRGBA>);
     pcl::io::loadPCDFile ("/home/lorenzo/ros_ws/cubo.pcd" , *cloud_cubo);
     pcl::IterativeClosestPoint<pcl::PointXYZRGBA, pcl::PointXYZRGBA> icp;
     pcl::PointCloud<pcl::PointXYZRGBA> cloud_source_registered;
     // Set the input source and target
     icp.setInputCloud (cloudInput[n]);
     icp.setInputTarget (cloud_cubo);
     // Set the max correspondence distance to 5cm (e.g., correspondences with higher distances will be ignored)
     icp.setMaxCorrespondenceDistance (0.50);
     // Set the maximum number of iterations (criterion 1)
     icp.setMaximumIterations (100);
     // Set the transformation epsilon (criterion 2)
     icp.setTransformationEpsilon (1e-6);
     // Set the euclidean distance difference epsilon (criterion 3)
     icp.setEuclideanFitnessEpsilon (1);
     // Perform the alignment
     icp.align (cloud_source_registered);
     // Obtain the transformation that aligned cloud_source to cloud_source_registered matrice di rotazione 3x3
     Eigen::Matrix4f transformation = icp.getFinalTransformation ();
     tf::Matrix3x3 rot_matrix(transformation (0,0), transformation (0,1), transformation (0,2),
                              transformation (1,0), transformation (1,1), transformation (1,2),
                              transformation (2,0), transformation (2,1), transformation (2,2));
     printf ("\n");
     printf ("            | %6.3f %6.3f %6.3f | \n", rot_matrix [0][0], rot_matrix [0][1], rot_matrix [0][2]);
     printf ("   tf     = | %6.3f %6.3f %6.3f | \n", rot_matrix [1][0], rot_matrix [1][1], rot_matrix [1][2]);
     printf ("            | %6.3f %6.3f %6.3f | \n", rot_matrix [2][0], rot_matrix [2][1], rot_matrix [2][2]);
     printf ("\n");
     tf::Quaternion q;
     rot_matrix.getRotation(q);
     printf ("\n");
     printf ("   q      = | %6.3f %6.3f %6.3f %6.3f | \n", q[0], q[1], q[2], q[3]);
     printf ("\n");
     transform[n].setRotation(q);
  }
  else if (prisma)
  {
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_prisma (new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::io::loadPCDFile ("/home/lorenzo/ros_ws/prisma.pcd" , *cloud_prisma);
    pcl::IterativeClosestPoint<pcl::PointXYZRGBA, pcl::PointXYZRGBA> icp;
    pcl::PointCloud<pcl::PointXYZRGBA> cloud_source_registered;
    // Set the input source and target
    icp.setInputCloud (cloudInput[n]);
    icp.setInputTarget (cloud_prisma);
    // Set the max correspondence distance to 5cm (e.g., correspondences with higher distances will be ignored)
    icp.setMaxCorrespondenceDistance (0.50);
    // Set the maximum number of iterations (criterion 1)
    icp.setMaximumIterations (100);
    // Set the transformation epsilon (criterion 2)
    icp.setTransformationEpsilon (1e-6);
    // Set the euclidean distance difference epsilon (criterion 3)
    icp.setEuclideanFitnessEpsilon (1);
    // Perform the alignment
    icp.align (cloud_source_registered);
    // Obtain the transformation that aligned cloud_source to cloud_source_registered matrice di rotazione 3x3
    Eigen::Matrix4f transformation = icp.getFinalTransformation ();
    tf::Matrix3x3 rot_matrix(transformation (0,0), transformation (0,1), transformation (0,2),
                             transformation (1,0), transformation (1,1), transformation (1,2),
                             transformation (2,0), transformation (2,1), transformation (2,2));
    printf ("\n");
    printf ("            | %6.3f %6.3f %6.3f | \n", rot_matrix [0][0], rot_matrix [0][1], rot_matrix [0][2]);
    printf ("   tf     = | %6.3f %6.3f %6.3f | \n", rot_matrix [1][0], rot_matrix [1][1], rot_matrix [1][2]);
    printf ("            | %6.3f %6.3f %6.3f | \n", rot_matrix [2][0], rot_matrix [2][1], rot_matrix [2][2]);
    printf ("\n");
    tf::Quaternion q;
    rot_matrix.getRotation(q);
    printf ("\n");
    printf ("   q      = | %6.3f %6.3f %6.3f %6.3f | \n", q[0], q[1], q[2], q[3]);
    printf ("\n");
    transform[n].setRotation(q);
  }
  else  // cilindro
  {
  transform[n].setRotation( tf::Quaternion(0.7, -0.7, 0.15, 0.18) );
  }
 // }
 // else{
 //   daAgg[n] = false;
 //   ROS_INFO("\nCluster %d saltato.", n);
 // }

}  // fine mega for

  fileOutput.close();

  // **************************** BROADCASTING TF *****************************

  while (nh.ok())
  {
    for(int i = 0; i < n_cluster ; i++){
      if(daAgg[i])
      {
       fileOutput << "/" << arrayNomi[i] << "\n";
       br[i].sendTransform(tf::StampedTransform(transform[i], ros::Time::now(), "camera_rgb_optical_frame", arrayNomi[i]));
      }
    }
  }
}
